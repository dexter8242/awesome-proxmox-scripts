#!/bin/bash

# Function to check if a command is available
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Function to read container IDs from file
read_container_ids() {
    local file="$1"
    if [ ! -f "$file" ]; then
        echo "Error: File $file does not exist or is not readable."
        exit 1
    fi

    while read -r line; do
        echo "$line"
    done < "$file"
}

# Function to update and upgrade a container
update_container() {
    local container_id="$1"
    echo "Processing container: $container_id"

    echo "Running apt update..."
    pct exec "$container_id" -- apt update 2>> "$log_file"
    if [ $? -ne 0 ]; then
        echo "Error: apt update failed for container $container_id"
        exit 1
    fi

    echo "Running apt upgrade..."
    pct exec "$container_id" -- apt upgrade -y 2>> "$log_file"
    if [ $? -ne 0 ]; then
        echo "Error: apt upgrade failed for container $container_id"
        exit 1
    fi
}

get_running_containers() {
	pct list | awk '/running/ {print $1}' 
}

# Main script
log_file="update_containers.log"
file_with_container_ids="running_containers.txt"

if ! command_exists pct; then
    echo "Error: command not found: pct"
    exit 1
fi

get_running_containers > running_containers.txt

echo "Updating and upgrading containers..."

# Read container IDs from file
container_ids=$(read_container_ids "$file_with_container_ids")

# Iterate over container IDs
for container_id in $container_ids; do
    update_container "$container_id"
done

echo "Done."
